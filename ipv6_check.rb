#!/usr/bin/env ruby
require 'resolv'
require 'fileutils'

def move(file)
  FileUtils.cp(file, 'public/index.html')
end

ipv6 = []
Resolv::DNS.open do |dns|
  ipv6 = dns.getresources("gitlab.com", Resolv::DNS::Resource::IN::AAAA)
end

FileUtils.cp('style.css', 'public/style.css')

if ipv6.any?
  puts "GitLab is IPv6 ready"
  move('yes.html')
else
  puts "GitLab is not IPv6 ready"
  move('no.html')
end
