## What?

Well, GitLab does not support IPv6 yet, which it should. :smile: So I've
deployed a [static site](https://zj-gitlab.gitlab.io/isgitlabipv6yet/) to check when it
does.

A pipeline schedule performs the check if it does support IPv6 that day, and if
so, the text will change to YES.
